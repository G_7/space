//Windows includes - DO NOT EDIT AS LOTS OF INTERDEPENDENCE
#define _USE_MATH_DEFINES

#include <math.h>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>	//Needed for console output (debugging)
#include <gl/freeglut.h>
#include <iostream>
#include <vector>
#include <list>
#include <ctime>

#ifdef WIN32
#include "gltools.h"
#include <windows.h>		// Must have for Windows platform builds
#include <gl\gl.h>			// Microsoft OpenGL headers (version 1.1 by themselves)
#include <gl\glu.h>			
#include "glm.h"
#endif



using namespace std;

#define IMAGE1      0
#define IMAGE2      1
#define IMAGE3      2
#define IMAGE4      3
#define IMAGE5      4
#define TEXTURE_COUNT 5
GLuint  textures[TEXTURE_COUNT];


const char *textureFiles[TEXTURE_COUNT] = {"HUD.tga", "jupiterC.tga","starField.tga", "background.tga", "Crystal1.tga"};




GLfloat  whiteLightBright[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat  redLight[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat  whiteLightLessBright[] = { 0.8f, 0.8f, 0.8f, 1.0f };
GLfloat	 lightPos[] = { 100.0f, 100.0f, 50.0f, 1.0f };



GLint iWidth, iHeight, iComponents;
GLenum eFormat;
// this is a pointer to memory where the image bytes will be held 
GLbyte *pBytes0;

GLdouble posx = 0, posy = 0, posz = -234, lookx = 0, looky = 0, lookz = 0, upx = 0, upy = 1, upz = 0; //look at values
float g_Rotatez = 0.0f;
float g_Rotatey = 0.0f;
float g_RotationSpeed = 0.5f;

bool yrot = false;
bool zrot = false;
bool hudOn = false;
float numCryst = 6;
GLfloat xval = 0;
vector<int> crystals;

// rotation values
GLfloat Rotation1 = 0.0f;
GLfloat Rotation2 = 0.0f;
int score = 0;


//------------------------------------//
class Hoops // hoops and crystal class
{
private:
	GLfloat x;
	GLfloat y;
	GLfloat z;
	GLfloat r = 255;
	GLfloat g = 0;
	GLfloat b = 255;
	bool collected = false; // is it collected



public:
	Hoops(GLfloat newx, GLfloat newy, GLfloat newz)
	{
		x = newx;
		y = newy;
		z = newz;
		
	}
	void setxyzpos(GLfloat newx, GLfloat newy, GLfloat newz)
	{
		x = newx;
		y = newy;
		z = newz;
	}
	void setRGB(GLfloat newr, GLfloat newg, GLfloat newb)
	{
		r = newr;
		g = newg;
		b = newb;
	}
	void setCollected(bool yes)                                         // getter and setter methods
	{
		collected = yes;
		
	}
	GLfloat getCollected()
	{
		return collected;
	}
	GLfloat getXpos()
	{
		return x;
	}
	GLfloat getYpos()
	{
		return y;
	}
	GLfloat getZpos()
	{
		return z;
	}
	GLfloat getrpos()
	{
		return r;
	}
	GLfloat getgpos()
	{
		return g;
	}
	GLfloat getbpos()
	{
		return b;
	}
	void DrawPlanet1(int image, GLfloat r, GLfloat R, GLfloat G, GLfloat B) // drawing planet
	{
		
		glColor3ub(R, G, B);
		
		glBindTexture(GL_TEXTURE_2D, textures[image]);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		double x, y, z, dTheta = 180 / 5, dLon = 360 / 5, degToRad = 3.141592665885 / 180;
		
		for (double lat = 0; lat <= 180; lat += dTheta)
		{

			glBegin(GL_QUAD_STRIP);
			for (double lon = 0; lon <= 360; lon += dLon)
			{


				//Vertex 1
				x = r * cos(lon * degToRad) * sin(lat * degToRad);
				y = r * sin(lon * degToRad) * sin(lat * degToRad);
				z = r * cos(lat * degToRad);
				glNormal3d(x, y, z);
				glTexCoord2d(lon / 360 - 0.25, lat / 180);
				glVertex3d(x, y, z);


				//Vetex 2
				x = r * cos(lon * degToRad) * sin((lat + dTheta) * degToRad);
				y = r * sin(lon * degToRad) * sin((lat + dTheta) * degToRad);
				z = r * cos((lat + dTheta) * degToRad);
				glNormal3d(x, y, z);
				glTexCoord2d(lon / 360 - 0.25, (lat + dTheta - 1) / (180));
				glVertex3d(x, y, z);


				//Vertex 3
				x = r * cos((lon + dLon) * degToRad) * sin((lat)*degToRad);
				y = r * sin((lon + dLon) * degToRad) * sin((lat)*degToRad);
				z = r * cos((lat)*degToRad);
				glNormal3d(x, y, z);
				glTexCoord2d((lon + dLon) / (360) - 0.25, (lat) / 180);
				glVertex3d(x, y, z);


				//Vertex 4
				x = r * cos((lon + dLon) * degToRad) * sin((lat + dTheta) * degToRad);
				y = r * sin((lon + dLon) * degToRad) * sin((lat + dTheta) * degToRad);
				z = r * cos((lat + dTheta) * degToRad);
				glNormal3d(x, y, z);
				glTexCoord2d((lon + dLon) / 360 - 0.25, (lat + dTheta) / (180));
				glVertex3d(x, y, z);


			}
			glEnd();

		}
		
	}
	void DrawTorus(int image) // drawing torus using equation of torus
	{
		glFrontFace(GL_CW);
		glBindTexture(GL_TEXTURE_2D, textures[image]); //texturing torus
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		
		int numc = 50, numt = 50;
		
		double TWOPI = 2 * M_PI;
		
		for (int i = 0; i < numc; i++) {
			glBegin(GL_QUAD_STRIP);
			for (int j = 0; j <= numt; j++) {
				for (int k = 1; k >= 0; k--) {

					double s = (i + k) % numc + 0.5;
					double t = j % numt;

					double x = (1 + 0.1 * cos(s * TWOPI / numc)) * cos(t * TWOPI / numt);
					double y = (1 + 0.1 * cos(s * TWOPI / numc)) * sin(t * TWOPI / numt);
					double z = 0.1 * sin(s * TWOPI / numc);
					double u = (i + k) / (float)numc;
					double v = t / (float)numt;
					glTexCoord2d(u, v); //texture coords
					glNormal3f(2 * x, 2 * y, 2 * z);
					glVertex3d(2 * x, 2 * y, 2 * z);
				}
			}
			glEnd();
			
		}
		glFrontFace(GL_CCW);
	}
	void DrawCrystals(GLfloat x, GLfloat y, GLfloat z) // drawing all
	{

		glPushMatrix();

		
		glRotatef(-Rotation2, 0.0f, 0.0f, 1.0f); // rotatimg the torus
		glScalef(10.0f, 10.0f, 10.0f);
		
		glColor3ub(0, 0, 255);
		
		DrawTorus(IMAGE3);
		
		glRotatef(Rotation1, 0.0f, 0.0f, 1.0f);
		

		
		
		glDisable(GL_LIGHTING);
		glEnable(GL_LIGHTING);
	
		
		glPopMatrix();
	}
	
	void DrawWhole(GLfloat r, GLfloat g, GLfloat b)
	{
		glPushMatrix();
		glTranslatef(x, y, z);
		DrawCrystals(0, 0, 0);
		
		if (collected == false)
		{
			glPushMatrix();
			glRotatef(Rotation2, 0.0f, 1.0f, 0.0f); //rotating the crystals
			DrawPlanet1(IMAGE5, 10, r, g, b);
			glPopMatrix();
		}
		
		glPopMatrix();
	}

	bool Collision(GLfloat x, GLfloat y, GLfloat z) //collision detection - point to sphere
	{
		float distance = sqrt((posx - x) * (posx - x) +
			(posy - y) * (posy - y) +
			(posz - (z)) * (posz - (z)));
		
		if (distance < 20) //20 - radius
		{
			
			return true;
		}
		else
		{
			
			return false;
		}

	}

};

void reset() // reset values 
{
	numCryst = 6;
	posx = 0;
	posy = 0;
	posz = -234;
	lookx = 0;
	looky = 0;
	lookz = 0;
	g_Rotatez = 0;
	g_Rotatey = 0;
}
// Called to draw scene

void DrawPlanet(int image, GLfloat r)
{
	
	glColor3ub(255, 255, 255);
	
	glBindTexture(GL_TEXTURE_2D, textures[image]);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	double x, y, z, dTheta = 180 / 20, dLon = 360 / 20, degToRad = 3.141592665885 / 180;
	
	for (double lat = 0; lat <= 180; lat += dTheta)
	{

		glBegin(GL_QUAD_STRIP);
		for (double lon = 0; lon <= 360; lon += dLon)
		{


			//Vertex 1
			x = r * cos(lon * degToRad) * sin(lat * degToRad);
			y = r * sin(lon * degToRad) * sin(lat * degToRad);
			z = r * cos(lat * degToRad);
			glNormal3d(x, y, z);
			glTexCoord2d(lon / 360 - 0.25, lat / 180);
			glVertex3d(x, y, z);


			//Vetex 2
			x = r * cos(lon * degToRad) * sin((lat + dTheta) * degToRad);
			y = r * sin(lon * degToRad) * sin((lat + dTheta) * degToRad);
			z = r * cos((lat + dTheta) * degToRad);
			glNormal3d(x, y, z);
			glTexCoord2d(lon / 360 - 0.25, (lat + dTheta - 1) / (180));
			glVertex3d(x, y, z);


			//Vertex 3
			x = r * cos((lon + dLon) * degToRad) * sin((lat)*degToRad);
			y = r * sin((lon + dLon) * degToRad) * sin((lat)*degToRad);
			z = r * cos((lat)*degToRad);
			glNormal3d(x, y, z);
			glTexCoord2d((lon + dLon) / (360) - 0.25, (lat) / 180);
			glVertex3d(x, y, z);


			//Vertex 4
			x = r * cos((lon + dLon) * degToRad) * sin((lat + dTheta) * degToRad);
			y = r * sin((lon + dLon) * degToRad) * sin((lat + dTheta) * degToRad);
			z = r * cos((lat + dTheta) * degToRad);
			glNormal3d(x, y, z);
			glTexCoord2d((lon + dLon) / 360 - 0.25, (lat + dTheta) / (180));
			glVertex3d(x, y, z);


		}
		glEnd();

	}
	
}

void keyboard(unsigned char key, int x, int y) 
{

	switch (key)
	{
	case 'e':
		if (posz <= 234) //stops moving past the border
		{
			posz += 2;
			lookz += 2;

		}
		
		break;
	case 'a':
		yrot = false;

		if (posx <= 200)
		{
			posx += 2;
			lookx += 2;
		}
		break;
	case 's':
		if (posz >= -234) 
		{
			posz -= 2;
			lookz -= 2;

		}
		
		break;
	case 'd':
		

		yrot = false;

		if (posx >= -200)
		{
			posx -= 2;
			lookx -= 2;
		}
		
		break;
	case 'z':
		g_Rotatey += g_RotationSpeed;
		break;

	case 'x':
		g_Rotatey -= g_RotationSpeed;
		break;

	}
	if (key == 'h') 
	{

		if (hudOn == false)
		{
			hudOn = true;
		}
		else if (hudOn == true)                                              // key controls
		{
			hudOn = false;
		}
		
	}

	glutPostRedisplay();
	
}

void specialKeyboard(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_RIGHT:
		
		g_Rotatez += g_RotationSpeed; // rotating the ship
		

		
		
		break;
	case GLUT_KEY_LEFT:
		
		g_Rotatez -= g_RotationSpeed;
		
		
		break;
	case GLUT_KEY_UP:
		
		
		if (posx <= 200 && posy <= 200 && posz <= 200) //stops car moving past the border
		{
			posy += 2;
			looky += 2;

		}
		
		break;

	case GLUT_KEY_DOWN:
		
		
		if (posy >= -200) 
		{
			posy -= 2;
			looky -= 2;

		}
		break;
	}
	glutPostRedisplay();
}




// Called to draw scene

vector<Hoops> hoops;
void Next(vector<Hoops> &hoops, vector<int> &crystals) // making the next cystal change color
{
	
	
	
	int val = rand() % crystals.size();
	
	int cryst = crystals[val];
	
	hoops[cryst - 1].setRGB(255, 0, 0);
	crystals.erase(crystals.begin() + val);
	int num = crystals.size();
	crystals.resize(num);
	
}
void DrawCrystal()
{
	
	
	DrawPlanet(IMAGE2, 10);
}
void HUD(int image) // hud
{
	
	glEnable(GL_TEXTURE_2D);
	
	glColor3f(0.8, 0.8, 0.8);
	glEnable(GL_TEXTURE_2D);
	//bind the texture 
	glBindTexture(GL_TEXTURE_2D, textures[image]);
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-1, 1, 0);
	glTexCoord3f(1.0, 0.0, 0.0);
	glVertex3f(-1, -1, 0);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(1, -1, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(1, 1, 0);
	glEnd();
	
}
void scoredisplay(int posx, int posy, int posz, int space_char, int& scorevar)
{
	if (scorevar <= 0) //stops score from going bellow 0
	{
		scorevar = 0;
	}

	int j = 0, p, k;
	GLvoid* font_style1 = GLUT_BITMAP_TIMES_ROMAN_24; //24-point proportional spaced Times Roman font

	p = scorevar;
	j = 0;
	k = 0;
	while (p > 9)
	{
		k = p % 10;
		glRasterPos3f((posx - (j * space_char)), posy, posz);//Specify the raster position for pixel operations.
		glutBitmapCharacter(font_style1, 48 + k); //Draw the characters one by one
		j++;
		p /= 10;
	}
	glRasterPos3f((posx - (j * space_char)), posy, posz); //Specify the raster position for pixel operations.
	glutBitmapCharacter(font_style1, 48 + p); //glutBitmapCharacter renders the character in the named bitmap font

}



void DrawHUD() // 2d layer for the hud
{
	int y = 68;
	glDisable(GL_DEPTH_TEST);
	
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(-1, 1, -1, 1, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	HUD(IMAGE1);
	
	
	glDisable(GL_BLEND);
	
	

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
	
}
void DrawText(const char *text, int length, int x, int y) // drawwwing the text
{


	glMatrixMode(GL_PROJECTION);
	double* matrix = new double[16];
	glGetDoublev(GL_PROJECTION_MATRIX, matrix);
	glLoadIdentity();
	glOrtho(0, 800, 0, 600, -5, 5);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	glLoadIdentity();
	glRasterPos2i(x, y);
	for (int i = 0; i < length; i++) 
	{
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
	}
	scoredisplay(150, 550, 0, 10, score);
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(matrix);
	glMatrixMode(GL_MODELVIEW);
}
void RenderScene(void)
{
 

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	gluLookAt(posx, posy, posz, lookx, looky, lookz, upx, upy, upz); // glut lookat

	
	glRotatef(-g_Rotatez, 0, 0, 1); //rotate the ship
	glRotatef(-g_Rotatey, 0, 1, 0);
	
	

	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	
	srand(time(NULL));
	if (crystals.size() < 1)  //makeing hoops and crystals
	{
		
		crystals.push_back(2);
		crystals.push_back(3);
		crystals.push_back(4);
		crystals.push_back(5);
		crystals.push_back(6);
	}
	
	if (hoops.size() < 1) 
	{
		for (int i = 0; i < 6; ++i)
		{
			int x = rand() % 201 + (-100);
			int y = rand() % 441 + (-220);
			int z = rand() % 281 + (-50);
			
			hoops.push_back(Hoops(x, y, z));
			hoops[0].setRGB(255, 0, 0);
		}
	}
	
	for (Hoops& h : hoops) 
	{
		h.DrawWhole(h.getrpos(), h.getgpos(), h.getbpos()); // drawing hoops and crystals
		
		if (h.getCollected() == false) 
		{
			if (h.Collision(h.getXpos(), h.getYpos(), h.getZpos()) == true)
			{
				cout << "Crystal Detected" << endl;
				h.setCollected(true); // means its collected 
				
				
				numCryst -= 1;
				cout << "crystals left: " << numCryst << endl;
				if (numCryst >= 1)
				{
					Next(hoops, crystals); // collision
				}
				score += 1; // increasing score
				
			}
		}
		if (numCryst <= 0)
		{
			hoops.clear();
			reset(); // when there are no more crystals reaload
		}
		
			
	
	}
	
	
	glPushMatrix();
	glTranslatef(-70, 0, 0);
	glRotatef(90, 1, 0, 0);
	glRotatef(Rotation2, 0, 0, 1);
	DrawPlanet(IMAGE2, 10);
	glPopMatrix();
	DrawPlanet(IMAGE4, 310);
	
	
	if (hudOn == true)  // draw hid
	{
		DrawHUD();
		

	}
	
	string text;
	text = "Crystals Collected: ";
	DrawText(text.data(), text.size(), 10, 550); // draw score
	
	glutSwapBuffers();
}



void SetupRC()
{
    //textures

    GLuint texture;
    // allocate a texture name
    glGenTextures( 1, &texture );
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	
    // Load textures in a for loop
    glGenTextures(TEXTURE_COUNT, textures);
    //puts the texture into OpenGL texture memory

    for(int iLoop = 0; iLoop < TEXTURE_COUNT; iLoop++)
    {
        // Bind to next texture object
        glBindTexture(GL_TEXTURE_2D, textures[iLoop]);
        
        // Load texture data, set filter and wrap modes
        pBytes0 = gltLoadTGA(textureFiles[iLoop],&iWidth, &iHeight,
                             &iComponents, &eFormat);
        
        glTexImage2D(GL_TEXTURE_2D, 0, iComponents, iWidth, iHeight, 0, eFormat, GL_UNSIGNED_BYTE, pBytes0);
        
            //set up texture parameters
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
         glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
       
        free(pBytes0);
    }
    
	//enable texture
    glEnable(GL_TEXTURE_2D);

    
	glEnable(GL_DEPTH_TEST);	// Hidden surface removal    
    glFrontFace(GL_CCW);       // Counter clock-wise polygons face out


    // Light 
	
								
	glEnable(GL_POINT_SMOOTH);
	
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, whiteLightBright);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, redLight);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glEnable(GL_LIGHT0);

	// Enable colour tracking
	glEnable(GL_COLOR_MATERIAL);

	// Set Material properties to follow glColor values
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);


	glClearColor(0.0f, 0.0f, 0.03f, 1.0f);
}


void TimerFunc(int value)
{
	
	Rotation2 += 2.0f; // rotate 
	

	
	Rotation1 += 1.0f;
	

	glutPostRedisplay();
	glutTimerFunc(25, TimerFunc, 1);
}

void ChangeSize(int w, int h)
{
	GLfloat fAspect;

	// Prevent a divide by zero
	if (h == 0)
		h = 1;

	// Set Viewport to window dimensions
	glViewport(0, 0, w, h);

	// Calculate aspect ratio of the window
	fAspect = (GLfloat)w / (GLfloat)h;

	// Set the perspective coordinate system
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// field of view of 45 degrees, near and far planes 1.0 and 425
	gluPerspective(45.0f, fAspect, 1.0f, 600.0);
	// Modelview matrix reset
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


int main(int argc, char* argv[])
{
	cout << " This is a 3D space game where you must control the first person character\n and move through space collecting all the crystals located in the spinning hoops" << endl;
	cout << "\n When all the crystals have been collected the game will reset \n\n Controls: " << endl;
	cout << "\n Left and Right Arrow: up and down \n Left and Right Arrow: roll ship anticlockwise and roll clockwise \n w, s: forwardand back \n a, d: strafe leftand strafe right \n z, x: rotate left / right on the y axis \n h: turn HUD onand off" << endl;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(1066, 600);
	glutCreateWindow("Solar System");
	glutReshapeFunc(ChangeSize);
	glutDisplayFunc(RenderScene);
	glutTimerFunc(25, TimerFunc, 1);
	SetupRC();
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialKeyboard);
	glutMainLoop();

	
	return 0;
}






